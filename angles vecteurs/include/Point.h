#include "SDL2/SDL.h"

class Point{
	SDL_Renderer* renderer;
	int x;
	int y;
	int thickness;
public:
	Point();
	Point(bool rnd,SDL_Renderer** renderer,int x,int y,int thickness);
	int getX();
	int getY();
	void draw();
	double controleDistance(int x,int y);
	void move(int x,int y);
};