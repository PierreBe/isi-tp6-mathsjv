#include <iostream>
#include "SDL2/SDL.h"
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <vector>
#include <math.h>
#define _USE_MATH_DEFINES

#include "Point.h"

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

#define HEIGHT 600
#define WIDTH 800

int main(int argc,char* argv[]){
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;
	SDL_Event event;
	SDL_bool done = SDL_FALSE;

	srand (time(NULL));

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		return 1;
	}
	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer) != 0) {
		return 2;
	}

	bool mouseButtonDown=false;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	Point pointA(true,&renderer,WIDTH,HEIGHT,50);
	Point pointB(true,&renderer,WIDTH,HEIGHT,50);
	Point pointC(true,&renderer,WIDTH,HEIGHT,50);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
	pointA.draw();
	pointB.draw();
	pointC.draw();
	SDL_RenderDrawLine(renderer,pointA.getX(),pointA.getY(),pointB.getX(),pointB.getY());
	SDL_RenderDrawLine(renderer,pointA.getX(),pointA.getY(),pointC.getX(),pointC.getY());
	SDL_RenderPresent(renderer);

	while (!done) {

		//

		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		while(SDL_PollEvent(&event)){
			if(event.type==SDL_MOUSEBUTTONDOWN&&event.button.button==SDL_BUTTON_LEFT){
				mouseButtonDown=true;
			}
			else if(event.type==SDL_MOUSEBUTTONUP&&event.button.button==SDL_BUTTON_LEFT){
				mouseButtonDown=false;
			}else if(event.type==SDL_MOUSEMOTION&&mouseButtonDown){

				SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
				SDL_RenderClear(renderer);

				pointC.move(event.motion.x,event.motion.y);
				
				//
				float membreSup1=((pointB.getX()-pointA.getX())*(pointC.getX()-pointA.getX())+(pointB.getY()-pointA.getY())*(pointC.getY()-pointA.getY()));
				float membreInf1=(pointB.controleDistance(pointA.getX(),pointA.getY())*pointC.controleDistance(pointA.getX(),pointA.getY()));
				float cosAlpha=(float)((pointB.getX()-pointA.getX())*(pointC.getX()-pointA.getX())+(pointB.getY()-pointA.getY())*(pointC.getY()-pointA.getY()))/(pointB.controleDistance(pointA.getX(),pointA.getY())*pointC.controleDistance(pointA.getX(),pointA.getY()));
				float alpha1=acos(((pointB.getX()-pointA.getX())*(pointC.getX()-pointA.getX())+(pointB.getY()-pointA.getY())*(pointC.getY()-pointA.getY()))/(pointB.controleDistance(pointA.getX(),pointA.getY())*pointC.controleDistance(pointA.getX(),pointA.getY())));
				std::cout<<"Acos("<<membreSup1<<" / "<<membreInf1<<") = Acos("<<cosAlpha<<")= "<<alpha1<<" rad = "<<alpha1/M_PI*180<<" °"<<std::endl;
				//

				//
				float membreSup2=(pointB.getX()-pointA.getX())*(pointC.getY()-pointA.getY())-(pointB.getY()-pointA.getY())*(pointC.getX()-pointA.getX());
				float membreInf2=pointB.controleDistance(pointA.getX(),pointA.getY())*pointC.controleDistance(pointA.getX(),pointA.getY());
				float sinAlpha=((pointB.getX()-pointA.getX())*(pointC.getY()-pointA.getY())-(pointB.getY()-pointA.getY())*(pointC.getX()-pointA.getX()))/(pointB.controleDistance(pointA.getX(),pointA.getY())*pointC.controleDistance(pointA.getX(),pointA.getY()));
				float alpha2=asin(((pointB.getX()-pointA.getX())*(pointC.getY()-pointA.getY())-(pointB.getY()-pointA.getY())*(pointC.getX()-pointA.getX()))/(pointB.controleDistance(pointA.getX(),pointA.getY())*pointC.controleDistance(pointA.getX(),pointA.getY())));
				std::cout<<"Asin("<<membreSup2<<" / "<<membreInf2<<") = Acos("<<sinAlpha<<")= "<<alpha2<<" rad = "<<alpha2/M_PI*180<<" °"<<std::endl;
				//

				SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
				pointA.draw();
				pointB.draw();
				SDL_RenderDrawLine(renderer,pointA.getX(),pointA.getY(),pointB.getX(),pointB.getY());
				SDL_RenderDrawLine(renderer,pointA.getX(),pointA.getY(),pointC.getX(),pointC.getY());
				int r=0,g=0,b=0;
				if(membreSup1<0){
				//if(alpha1<M_PI/2){
					r=255;
				}else{
					b=255;
				}
				if(membreSup2<0){
				//if(alpha2<0){
					g=255;
				}else{
				}
				SDL_SetRenderDrawColor(renderer, r, g, b, SDL_ALPHA_OPAQUE);
				pointC.draw();
				SDL_RenderPresent(renderer);


			}else if(event.type == SDL_QUIT){
				done = SDL_TRUE;
			}
		}
	}

	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	return 0;
}