#include "SDL2/SDL.h"
#include "Point.h"

class Ligne{
	SDL_Renderer* renderer;
	Point* pointDebut;
	Point* pointFin;
	int r=255,g=255,b=255;
public:
	Ligne(SDL_Renderer** renderer,Point* pointDebut,Point* pointFin);
	void draw();
	bool isAGauche(Point point);
	void setColor(int r,int g,int b);
};