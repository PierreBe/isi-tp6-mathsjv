#include "Point.h"

class Triangle{
	Point* point1;
	Point* point2;
	Point* point3;
public:
	Triangle(Point* point1,Point* point2,Point* point3);
	bool isInside(Point point);
};