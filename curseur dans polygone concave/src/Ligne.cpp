#include "Ligne.h"
#include <stdlib.h>
#include "SDL2/SDL.h"
#include <math.h>

Ligne::Ligne(SDL_Renderer** renderer,Point* pointDebut,Point* pointFin){
	this->renderer=*renderer;
	this->pointDebut=pointDebut;
	this->pointFin=pointFin;
}

void Ligne::draw(){
	SDL_SetRenderDrawColor(renderer,this->r,this->g,this->b,SDL_ALPHA_OPAQUE);
	SDL_RenderDrawLine(this->renderer,(*this->pointDebut).getX(),(*this->pointDebut).getY(),(*this->pointFin).getX(),(*this->pointFin).getY());
	//SDL_RenderPresent(this->renderer);
}

bool Ligne::isAGauche(Point point){
	if(((*this->pointFin).getX()-(*this->pointDebut).getX())*(point.getY()-(*this->pointDebut).getY())-((*this->pointFin).getY()-(*this->pointDebut).getY())*(point.getX()-(*this->pointDebut).getX())>0)
		return true;
	else
		return false;
}

void Ligne::setColor(int r,int g,int b){
	this->r=r;
	this->g=g;
	this->b=b;
}