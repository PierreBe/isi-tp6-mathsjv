#include <iostream>
#include "SDL2/SDL.h"
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <vector>
#include <math.h>
#define _USE_MATH_DEFINES

#include "Point.h"
#include "Ligne.h"
#include "Triangle.h"

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

#define HEIGHT 600
#define WIDTH 800

int main(int argc,char* argv[]){
	SDL_Window* window=NULL;
	SDL_Renderer* renderer=NULL;
	SDL_Event event;
	SDL_bool done=SDL_FALSE;

	srand(time(NULL));

	if (SDL_Init(SDL_INIT_VIDEO)!=0){
		return 1;
	}
	if (SDL_CreateWindowAndRenderer(WIDTH,HEIGHT,0,&window,&renderer)!=0){
		return 2;
	}

	bool mouseButtonDown=false;

	SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	bool trouve;
	int indicePoint;
	std::vector<Point> pointArray;
	pointArray.push_back(Point(false,&renderer,WIDTH/4,HEIGHT/4,20));
	pointArray.push_back(Point(false,&renderer,WIDTH/2,HEIGHT/4,20));
	pointArray.push_back(Point(false,&renderer,WIDTH*3/4,HEIGHT/4,20));
	pointArray.push_back(Point(false,&renderer,WIDTH*3/4,HEIGHT*3/4,20));
	pointArray.push_back(Point(false,&renderer,WIDTH/2,HEIGHT*3/4,20));
	pointArray.push_back(Point(false,&renderer,WIDTH/4,HEIGHT*3/4,20));

	Point pointP(true,&renderer,WIDTH,HEIGHT,50);
	for(Point point:pointArray){
		point.draw();
	}
	pointP.draw();

	std::vector<Ligne> lineArray;
	for(int i=0;i<pointArray.size()-1;i++){
		lineArray.push_back(Ligne(&renderer,&pointArray[i],&pointArray[i+1]));
	}
	lineArray.push_back(Ligne(&renderer,&pointArray[pointArray.size()-1],&pointArray[0]));

	std::vector<Ligne> lineSupArray;
	lineSupArray.push_back(Ligne(&renderer,&pointArray[1],&pointArray[5]));
	lineSupArray.back().setColor(128,128,128);
	lineSupArray.push_back(Ligne(&renderer,&pointArray[2],&pointArray[4]));
	lineSupArray.back().setColor(128,128,128);
	lineSupArray.push_back(Ligne(&renderer,&pointArray[1],&pointArray[4]));
	lineSupArray.back().setColor(128,128,128);

	for(Ligne line:lineArray){
		line.draw();
	}
	for(Ligne line:lineSupArray){
		line.draw();
	}

	std::vector<Triangle> triangleArray;
	triangleArray.push_back(Triangle(&pointArray[0],&pointArray[1],&pointArray[5]));
	triangleArray.push_back(Triangle(&pointArray[1],&pointArray[2],&pointArray[4]));
	triangleArray.push_back(Triangle(&pointArray[3],&pointArray[4],&pointArray[2]));
	triangleArray.push_back(Triangle(&pointArray[4],&pointArray[5],&pointArray[1]));

	SDL_RenderPresent(renderer);

	while (!done) {

		//

		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		while(SDL_PollEvent(&event)){
			if(event.type==SDL_MOUSEBUTTONDOWN&&event.button.button==SDL_BUTTON_LEFT){
				trouve=false;
				indicePoint=0;
				while(!trouve&&(indicePoint<pointArray.size())){
					if(event.motion.x>pointArray[indicePoint].getX()-pointArray[indicePoint].getThickness()/2&&event.motion.y>pointArray[indicePoint].getY()-pointArray[indicePoint].getThickness()/2&&event.motion.x<pointArray[indicePoint].getX()+pointArray[indicePoint].getThickness()/2&&event.motion.y<pointArray[indicePoint].getY()+pointArray[indicePoint].getThickness()/2){
						mouseButtonDown=true;
						trouve=true;
					}
					indicePoint++;
				}
				indicePoint--;
				if(!trouve&&event.motion.x>pointP.getX()-pointP.getThickness()/2&&event.motion.y>pointP.getY()-pointP.getThickness()/2&&event.motion.x<pointP.getX()+pointP.getThickness()/2&&event.motion.y<pointP.getY()+pointP.getThickness()/2){
					mouseButtonDown=true;
				}
			}
			else if(event.type==SDL_MOUSEBUTTONUP&&event.button.button==SDL_BUTTON_LEFT){
				mouseButtonDown=false;
			}else if(event.type==SDL_MOUSEMOTION&&mouseButtonDown){


				SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
				SDL_RenderClear(renderer);

				if(trouve){
					pointArray[indicePoint].move(event.motion.x,event.motion.y);
				}else{
					pointP.move(event.motion.x,event.motion.y);
				}

				/*bool in=true;
				int indiceLigne=0;
				while(in&&(indiceLigne<lineArray.size())){
					if(!lineArray[indiceLigne].isAGauche(pointP))
						in=false;
					indiceLigne++;
				}*/
				bool in=false;
				int indiceTriangle=0;
				while(!in&&(indiceTriangle<triangleArray.size())){
					if(triangleArray[indiceTriangle].isInside(pointP))
						in=true;
					indiceTriangle++;
				}
				if(in){
					pointP.setColor(0,255,0);
				}else{
					pointP.setColor(255,0,0);
				}
				pointP.draw();

				for(Point point:pointArray){
					point.draw();
				}
				for(Ligne line:lineArray){
					line.draw();
				}
				for(Ligne line:lineSupArray){
					line.draw();
				}

				SDL_RenderPresent(renderer);


			}else if(event.type == SDL_QUIT){
				done = SDL_TRUE;
			}
		}
	}

	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	return 0;
}