#include "Triangle.h"
#include <stdlib.h>
#include <math.h>

Triangle::Triangle(Point* point1,Point* point2,Point* point3){
	this->point1=point1;
	this->point2=point2;
	this->point3=point3;
}

bool Triangle::isInside(Point point){
	if(((*this->point2).getX()-(*this->point1).getX())*(point.getY()-(*this->point1).getY())-((*this->point2).getY()-(*this->point1).getY())*(point.getX()-(*this->point1).getX())>0&&
		((*this->point3).getX()-(*this->point2).getX())*(point.getY()-(*this->point2).getY())-((*this->point3).getY()-(*this->point2).getY())*(point.getX()-(*this->point2).getX())>0&&
		((*this->point1).getX()-(*this->point3).getX())*(point.getY()-(*this->point3).getY())-((*this->point1).getY()-(*this->point3).getY())*(point.getX()-(*this->point3).getX())>0)
		return true;
	else
		return false;
}