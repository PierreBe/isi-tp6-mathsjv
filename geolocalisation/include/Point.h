#include "SDL2/SDL.h"

class Point{
	SDL_Renderer* renderer;
	int x;
	int y;
	int thickness;
	double distance;
	int distance2;
	int r=255;
	int g=255;
	int b=255;
	int indice=0;
public:
	Point(SDL_Renderer** renderer);
	Point(bool rnd,SDL_Renderer** renderer,int x,int y,int thickness);
	int getX();
	int getY();
	void draw();
	double controleDistance(int x,int y);
	double controleDistance(Point point);
	double getDistance();
	double controleDistance2(int x,int y);
	double controleDistance2(Point point);
	double getDistance2();
	void move(int x,int y);
	void changeColor();
	bool isInPerimeter(int x,int y);
	bool isInPerimeter(Point point);
	bool isOnPerimeter(int x,int y);
	bool isOnPerimeter(Point point);
	bool isInPerimeter2(int x,int y);
	bool isInPerimeter2(Point point);
	bool isOnPerimeter2(int x,int y);
	bool isOnPerimeter2(Point point);
};