#include <iostream>
#include "SDL2/SDL.h"
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <vector>
#include <math.h>
#define _USE_MATH_DEFINES

#include "Point.h"

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

#define HEIGHT 600
#define WIDTH 800

int arrondi(double x){
	if(x-(int)x<0.5)
		return x;
	else
		return x+1;
}

void trianguler(int* xP, int* yP,Point point1,Point point2,Point point3,SDL_Renderer** renderer){
	*xP=0;
	*yP=0;
	int i=0;
	bool trouve=false;
	double angle=0;
	int x=0,y=0;
	Point point(renderer);
	while(!trouve&&i<2*M_PI*point1.getDistance()){
		angle=(double)i/point1.getDistance();
		x=arrondi(point1.getX()+point1.getDistance()*cos(angle));
		y=arrondi(point1.getY()-point1.getDistance()*sin(angle));
		point.move(x,y);
		point.draw();
		SDL_RenderPresent(*renderer);
		if(point2.isOnPerimeter2(point)&&point3.isOnPerimeter2(point)){
			*xP=x;
			*yP=y;
			trouve=true;
		}
		i++;
	}
}


int main(int argc,char* argv[]){
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;
	SDL_Event event;
	SDL_bool done = SDL_FALSE;

	srand (time(NULL));

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		return 1;
	}
	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer) != 0) {
		return 2;
	}

	bool mouseButtonDown=false;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	Point pointA(true,&renderer,WIDTH,HEIGHT,10);
	Point pointB(true,&renderer,WIDTH,HEIGHT,10);
	Point pointC(true,&renderer,WIDTH,HEIGHT,10);
	Point pointP(true,&renderer,WIDTH,HEIGHT,10);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
	pointA.draw();
	pointB.draw();
	pointC.draw();
	pointP.draw();
	SDL_RenderDrawLine(renderer,pointA.getX(),pointA.getY(),pointP.getX(),pointP.getY());
	SDL_RenderDrawLine(renderer,pointB.getX(),pointB.getY(),pointP.getX(),pointP.getY());
	SDL_RenderDrawLine(renderer,pointC.getX(),pointC.getY(),pointP.getX(),pointP.getY());
	SDL_RenderPresent(renderer);

	int xP=0,yP=0;

	while (!done) {

		//

		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		while(SDL_PollEvent(&event)){
			if(event.type==SDL_MOUSEBUTTONDOWN&&event.button.button==SDL_BUTTON_LEFT){
				mouseButtonDown=true;
			}
			else if(event.type==SDL_MOUSEBUTTONUP&&event.button.button==SDL_BUTTON_LEFT){
				mouseButtonDown=false;

				/*std::cout<<"P.x = "<<pointP.getX()<<", P.y = "<<pointP.getY()<<std::endl;
				std::cout<<std::endl;
				std::cout<<"PA = "<<pointA.controleDistance(pointP)<<std::endl;
				std::cout<<"PB = "<<pointB.controleDistance(pointP)<<std::endl;
				std::cout<<"PC = "<<pointC.controleDistance(pointP)<<std::endl;
				std::cout<<"PA^2 = "<<pointA.controleDistance2(pointP)<<std::endl;
				std::cout<<"PB^2 = "<<pointB.controleDistance2(pointP)<<std::endl;
				std::cout<<"PC^2 = "<<pointC.controleDistance2(pointP)<<std::endl;
				std::cout<<std::endl;
				trianguler(&xP,&yP,pointA,pointB,pointC,&renderer);
				std::cout<<"xP = "<<xP<<", yP = "<<yP<<std::endl;
				std::cout<<std::endl;*/

			}else if(event.type==SDL_MOUSEMOTION&&mouseButtonDown){

				SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
				SDL_RenderClear(renderer);

				pointP.move(event.motion.x,event.motion.y);

				SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
				pointA.draw();
				pointB.draw();
				pointC.draw();
				pointP.draw();
				SDL_RenderDrawLine(renderer,pointA.getX(),pointA.getY(),pointP.getX(),pointP.getY());
				SDL_RenderDrawLine(renderer,pointB.getX(),pointB.getY(),pointP.getX(),pointP.getY());
				SDL_RenderDrawLine(renderer,pointC.getX(),pointC.getY(),pointP.getX(),pointP.getY());
				SDL_RenderPresent(renderer);

				std::cout<<"P.x = "<<pointP.getX()<<", P.y = "<<pointP.getY()<<std::endl;
				std::cout<<std::endl;
				std::cout<<"PA = "<<pointA.controleDistance(pointP)<<std::endl;
				std::cout<<"PB = "<<pointB.controleDistance(pointP)<<std::endl;
				std::cout<<"PC = "<<pointC.controleDistance(pointP)<<std::endl;
				std::cout<<"PA^2 = "<<pointA.controleDistance2(pointP)<<std::endl;
				std::cout<<"PB^2 = "<<pointB.controleDistance2(pointP)<<std::endl;
				std::cout<<"PC^2 = "<<pointC.controleDistance2(pointP)<<std::endl;
				std::cout<<std::endl;
				trianguler(&xP,&yP,pointA,pointB,pointC,&renderer);
				std::cout<<"xP = "<<xP<<", yP = "<<yP<<std::endl;
				std::cout<<std::endl;

			}else if(event.type == SDL_QUIT){
				done = SDL_TRUE;
			}
		}
	}

	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	return 0;
}