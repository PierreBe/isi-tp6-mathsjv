#include "SDL2/SDL.h"

class Aiguille{
	SDL_Renderer* renderer;
	int x1,y1,x2,y2;
	int thickness;
	int r=255,g=255,b=255,indice=0;
public:
	Aiguille(SDL_Renderer** renderer,int x1,int y1,int x2,int y2);
	void draw();
	void move(int x2,int y2);
};