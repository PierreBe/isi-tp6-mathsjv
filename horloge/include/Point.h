#include "SDL2/SDL.h"

class Point{
	SDL_Renderer* renderer;
	int x;
	int y;
	int thickness;
	int r=255,g=255,b=255,indice=0;
public:
	Point();
	Point(bool rnd,SDL_Renderer** renderer,int x,int y,int thickness);
	int getX();
	int getY();
	void draw();
	double controleDistance(int x,int y);
	void move(int x,int y);
	void changeColor();
};