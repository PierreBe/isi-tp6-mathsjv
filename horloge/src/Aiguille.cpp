#include "Aiguille.h"
#include <stdlib.h>
#include "SDL2/SDL.h"
#include <math.h>

Aiguille::Aiguille(SDL_Renderer** renderer,int x1,int y1,int x2,int y2){
	this->renderer=*renderer;
	this->x1=x1;
	this->y1=y1;
	this->x2=x2;
	this->y2=y2;
}

void Aiguille::draw(){
	/*for(int i=-this->thickness/2;i<this->thickness/2;i++){
		for(int j=-this->thickness/2;j<this->thickness/2;j++){
			SDL_RenderDrawPoint(this->renderer,this->x+i,this->y+j);
		}
	}*/
	SDL_RenderDrawLine(this->renderer,this->x1,this->y1,this->x2,this->y2);
	SDL_RenderPresent(this->renderer);
}

void Aiguille::move(int x2,int y2){
	SDL_SetRenderDrawColor(renderer,0,0,0, SDL_ALPHA_OPAQUE);
	this->draw();
	this->x2=x2;
	this->y2=y2;
	SDL_SetRenderDrawColor(renderer,255,255,255, SDL_ALPHA_OPAQUE);
	this->draw();
}