#include "Point.h"
#include <stdlib.h>
#include "SDL2/SDL.h"
#include <math.h>

Point::Point(){}

Point::Point(bool rnd,SDL_Renderer** renderer,int x,int y,int thickness){
		this->renderer=*renderer;
	if(rnd){
		this->x=rand()%(x-thickness)+thickness/2;
		this->y=rand()%(y-thickness)+thickness/2;
	}else{
		this->x=x;
		this->y=y;
	}
		this->thickness=thickness;
}

int Point::getX(){
	return this->x;
}

int Point::getY(){
	return this->y;
}

void Point::draw(){
	SDL_SetRenderDrawColor(renderer, this->r, this->g, this->b, SDL_ALPHA_OPAQUE);
	/*for(int i=-this->thickness/2;i<this->thickness/2;i++){
		for(int j=-this->thickness/2;j<this->thickness/2;j++){
			SDL_RenderDrawPoint(this->renderer,this->x+i,this->y+j);
		}
	}*/
	SDL_RenderDrawPoint(this->renderer,this->x,this->y);
	SDL_RenderPresent(this->renderer);
}

double Point::controleDistance(int x,int y){
	return sqrt(pow(this->x-x,2)+pow(this->y-y,2));
}

void Point::move(int x,int y){
	this->x=x;
	this->y=y;
	this->draw();
}

void Point::changeColor(){
	if(this->indice==0){
		this->r=255;
		this->g=0;
		this->b=0;
	}else if(this->indice==1){
		this->r=0;
		this->g=255;
		this->b=0;
	}else if(this->indice==2){
		this->r=0;
		this->g=0;
		this->b=255;
	}else if(this->indice==3){
		this->r=255;
		this->g=255;
		this->b=0;
	}else if(this->indice==4){
		this->r=0;
		this->g=255;
		this->b=255;
	}else if(this->indice==5){
		this->r=255;
		this->g=0;
		this->b=255;
		this->indice=-1;
	}
	this->indice++;
}