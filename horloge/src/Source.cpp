#include <iostream>
#include "SDL2/SDL.h"
#include <math.h>
#include <time.h>
#include <thread>

#include "Point.h"
#include "Aiguille.h"

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

#define SIZE 500
#define CENTRE SIZE/2
#define R_SEC 200
#define R_MIN 150
#define R_HR 100
#define DUREE_SECONDE 1000

int main(int argc, char* argv[])
{
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;
	SDL_bool done = SDL_FALSE;
	SDL_Event event;

	if(SDL_Init(SDL_INIT_VIDEO)!=0){
		return 1;
	}
	if(SDL_CreateWindowAndRenderer(SIZE,SIZE,0,&window,&renderer)!=0){
		return 2;
	}

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);

	double marque=0;
	int cpt=0;
	Point pointMarque(false,&renderer,CENTRE,CENTRE-R_SEC+10,1);
	while(marque<2*M_PI-0.1){
		pointMarque.move(CENTRE+(R_SEC+10)*cos(M_PI/2-marque),CENTRE-(R_SEC+10)*sin(M_PI/2-marque));
		if(cpt%5==0){
			pointMarque.move(CENTRE+(R_SEC+10+1)*cos(M_PI/2-marque),CENTRE-(R_SEC+10+1)*sin(M_PI/2-marque));
			pointMarque.move(CENTRE+(R_SEC+10+2)*cos(M_PI/2-marque),CENTRE-(R_SEC+10+2)*sin(M_PI/2-marque));
			pointMarque.move(CENTRE+(R_SEC+10+3)*cos(M_PI/2-marque),CENTRE-(R_SEC+10+3)*sin(M_PI/2-marque));
		}
		marque+=M_PI/30;
		cpt++;
	}

	int seconde=0,minute=0,heure=0;
	Point pointSeconde(false,&renderer,CENTRE,CENTRE-R_SEC,1);
	Point pointMinute(false,&renderer,CENTRE,CENTRE-R_MIN,1);
	Point pointHeure(false,&renderer,CENTRE,CENTRE-R_HR,1);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
	pointSeconde.changeColor();
	pointMinute.changeColor();
	pointHeure.changeColor();
	pointSeconde.draw();
	pointMinute.draw();
	pointHeure.draw();

	Aiguille aiguilleSeconde(&renderer,CENTRE,CENTRE,CENTRE+(R_HR-20*1)*cos(M_PI/2-clock()*M_PI/30000),CENTRE-(R_HR-20*1)*sin(M_PI/2-clock()*M_PI/30000));
	Aiguille aiguilleMinute(&renderer,CENTRE,CENTRE,CENTRE+(R_HR-20*2)*cos(M_PI/2-clock()*M_PI/30000),CENTRE-(R_HR-20*2)*sin(M_PI/2-clock()*M_PI/30000));
	Aiguille aiguilleHeure(&renderer,CENTRE,CENTRE,CENTRE+(R_HR-20*3)*cos(M_PI/2-clock()*M_PI/30000),CENTRE-(R_HR-20*3)*sin(M_PI/2-clock()*M_PI/30000));

	clock_t t;
	t=0;

	while (!done) {

		//

		if(clock()>=t+DUREE_SECONDE){
			seconde++;
			if(seconde>59){
				seconde=0;
				pointSeconde.changeColor();
				minute++;
				if(minute>59){
					minute=0;
					pointMinute.changeColor();
					heure++;
					if(heure>11){
						heure=0;
						pointHeure.changeColor();
					}
				}
			}
			t+=DUREE_SECONDE;
		}

		aiguilleSeconde.move(CENTRE+(R_HR-20*1)*cos(M_PI/2-clock()*M_PI/30000),CENTRE-(R_HR-20*1)*sin(M_PI/2-clock()*M_PI/30000));
		aiguilleMinute.move(CENTRE+(R_HR-20*2)*cos(M_PI/2-clock()*M_PI/(30000*60)),CENTRE-(R_HR-20*2)*sin(M_PI/2-clock()*M_PI/(30000*60)));
		aiguilleHeure.move(CENTRE+(R_HR-20*3)*cos(M_PI/2-clock()*M_PI/(30000*60*12)),CENTRE-(R_HR-20*3)*sin(M_PI/2-clock()*M_PI/(30000*60*12)));

		pointSeconde.move(CENTRE+R_SEC*cos(M_PI/2-clock()*M_PI/30000),CENTRE-R_SEC*sin(M_PI/2-clock()*M_PI/30000));
		pointMinute.move(CENTRE+R_MIN*cos(M_PI/2-clock()*M_PI/(30000*60)),CENTRE-R_MIN*sin(M_PI/2-clock()*M_PI/(30000*60)));
		pointHeure.move(CENTRE+R_HR*cos(M_PI/2-clock()*M_PI/(30000*60*12)),CENTRE-R_HR*sin(M_PI/2-clock()*M_PI/(30000*60*12)));

		/*if(clock()>=t+DUREE_SECONDE){
			seconde++;
			if(seconde>59){
				seconde=0;
				pointSeconde.changeColor();
				minute++;
				if(minute>59){
					minute=0;
					pointMinute.changeColor();
					heure++;
					if(heure>11){
						heure=0;
						pointHeure.changeColor();
					}
					pointHeure.move(CENTRE+R_HR*cos(M_PI/2-heure*M_PI/6),CENTRE-R_HR*sin(M_PI/2-heure*M_PI/6));
				}
				pointMinute.move(CENTRE+R_MIN*cos(M_PI/2-minute*M_PI/30),CENTRE-R_MIN*sin(M_PI/2-minute*M_PI/30));
			}
			pointSeconde.move(CENTRE+R_SEC*cos(M_PI/2-seconde*M_PI/30),CENTRE-R_SEC*sin(M_PI/2-seconde*M_PI/30));
			t+=DUREE_SECONDE;
		}*/

		//std::cout<<clock()<<std::endl;

		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		
		while(SDL_PollEvent(&event)){
			if(event.type == SDL_QUIT){
				done = SDL_TRUE;
			}
		}
	}

	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}

	SDL_Quit();
	return 0;
}


//clock_t t;
//t = clock();
//t = clock() - t;