#include "SDL2/SDL.h"

class Point{
	SDL_Renderer* renderer;
	int x;
	int y;
	int thickness;
public:
	Point();
	Point(bool rnd,SDL_Renderer** renderer,int x,int y,int thickness);
	int getX();
	int getY();
	void draw();
	int controleDistance(int x,int y);
};