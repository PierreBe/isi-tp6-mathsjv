#include <iostream>
#include "SDL2/SDL.h"
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <vector>

#include "Point.h"

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

#define HEIGHT 600
#define WIDTH 800
#define NB_POINTS 50

int main(int argc,char* argv[]){
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;
	SDL_Event event;
	SDL_bool done = SDL_FALSE;

	srand (time(NULL));

	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		return 1;
	}
	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer) != 0) {
		return 2;
	}

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	SDL_SetRenderDrawColor(renderer, 96, 96, 96, SDL_ALPHA_OPAQUE);

	Point* pointPtr;
	std::vector<Point> pointArray;

	for(int i=0;i<NB_POINTS;i++){
		pointPtr=new Point(true,&renderer,WIDTH,HEIGHT,10);
		pointArray.push_back(*pointPtr);
	}

	for(Point pt:pointArray){
		pt.draw();
	}

	Point point=pointArray[0];

	/////////////////////////////////

	// Isobarycentre :
	int xIBC=0,yIBC=0;
	for(Point pt:pointArray){
		xIBC+=pt.getX();
		yIBC+=pt.getY();
	}
	xIBC/=pointArray.size();
	yIBC/=pointArray.size();
	Point iBC(false,&renderer,xIBC,yIBC,10);
	SDL_SetRenderDrawColor(renderer, 0, 0, 255, SDL_ALPHA_OPAQUE);
	iBC.draw();

	/////////////////////////////////

	while (!done) {

		//

		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		while(SDL_PollEvent(&event)){
			if(event.type == SDL_MOUSEMOTION){

				SDL_SetRenderDrawColor(renderer, 96, 96, 96, SDL_ALPHA_OPAQUE);
				point.draw();

				for(Point pt:pointArray){
					if(pt.controleDistance(event.motion.x,event.motion.y)<point.controleDistance(event.motion.x,event.motion.y))
						point=pt;
				}

				SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
				point.draw();

			}else if(event.type == SDL_QUIT){
				done = SDL_TRUE;
			}
		}
	}

	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	return 0;
}