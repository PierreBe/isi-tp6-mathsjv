#ifndef ASTRE_H
#define ASTRE_H

#include "SDL2/SDL.h"
#include <vector>

//#define G 6.6742e43
#define G 6.6742e-11

class Astre{
	SDL_Renderer* renderer;
	double px;
	double py;
	double vx;
	double vy;
	double ax;
	double ay;
	double rayon;
	double masse;
	int r=255;
	int g=255;
	int b=255;
	std::vector<Astre*> astres;
	//double G=6.6742e-11;
public:
	//Astre(Astre astre);
	Astre(SDL_Renderer** renderer,double px,double py,double vx,double vy,double ax,double ay,double rayon,double masse);
	SDL_Renderer* getRenderer();
	double getPX();
	double getPY();
	double getVX();
	double getVY();
	double getAX();
	double getAY();
	double getRayon();
	double getMasse();
	void setPX(double px);
	void setPY(double py);
	void setVX(double vx);
	void setVY(double vy);
	void setAX(double ax);
	void setAY(double ay);
	void setRayon(double rayon);
	void setMasse(double masse);
	void draw();
	void fill();
	void trace();
	double controleDistance2(double px,double py);
	double controleDistance2(Astre astre);
	void move(double px,double py);
	void move(double dt);
	void add(Astre* astre);
	void add(std::vector<Astre*> astres);
	void setColor(int r,int g,int b);
	//double getG();
	//void setG(double G);
};

#endif