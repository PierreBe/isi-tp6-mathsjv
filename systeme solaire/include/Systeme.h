#include "Astre.h"
#include <vector>

class Systeme{
	SDL_Renderer* renderer;
	int width;
	int height;
	double factor;
	std::vector<Astre*> systemeBase;
	std::vector<Astre*> systemeScale;
public:
	Systeme(SDL_Renderer** renderer,int width,int height);
	void add(std::vector<Astre*> systemeBase);
	void add(Astre* astre);
	void scale();
	void compute(int dt);
};