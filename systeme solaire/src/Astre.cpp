#include "Astre.h"
#include <stdlib.h>
#include <math.h>
//#include <iostream>

/*Astre::Astre(Astre astre){
	this->renderer=astre.getRenderer();
	this->px=astre.getPX();
	this->py=astre.getPY();
	this->vx=astre.getVX();
	this->vy=astre.getVY();
	this->ax=astre.getAX();
	this->ay=astre.getAY();
	this->rayon=astre.getRayon();
	this->masse=astre.getMasse();
	//this=new Astre(astre);
}*/

Astre::Astre(SDL_Renderer** renderer,double px,double py,double vx,double vy,double ax,double ay,double rayon,double masse){
	this->renderer=*renderer;
	this->px=px;
	this->py=py;
	this->vx=vx;
	this->vy=vy;
	this->ax=ax;
	this->ay=ay;
	this->rayon=rayon;
	this->masse=masse;
}

SDL_Renderer* Astre::getRenderer(){
	return this->renderer;
}

double Astre::getPX(){
	return this->px;
}

double Astre::getPY(){
	return this->py;
}

double Astre::getVX(){
	return this->vx;
}

double Astre::getVY(){
	return this->vy;
}

double Astre::getAX(){
	return this->ax;
}

double Astre::getAY(){
	return this->ay;
}

double Astre::getRayon(){
	return this->rayon;
}

double Astre::getMasse(){
	return this->masse;
}

void Astre::setPX(double px){
	this->px=px;
}

void Astre::setPY(double py){
	this->py=py;
}

void Astre::setVX(double vx){
	this->vx=vx;
}

void Astre::setVY(double vy){
	this->vy=vy;
}

void Astre::setAX(double ax){
	this->ax=ax;
}

void Astre::setAY(double ay){
	this->ay=ay;
}

void Astre::setRayon(double rayon){
	this->rayon=rayon;
}

void Astre::setMasse(double masse){
	this->masse=masse;
}

void Astre::draw(){
	SDL_SetRenderDrawColor(this->renderer,this->r,this->g,this->b,SDL_ALPHA_OPAQUE);
	int x=this->rayon;
	int r2=pow(this->rayon,2);
	int r21=0,r22=0;
	int e1=0,e2=0;
	for(int y=0;y<this->rayon*0.71;y++){
		r21=pow(x,2)+pow(y,2);
		r22=pow(x-1,2)+pow(y,2);
		e1=abs(r2-r21);
		e2=abs(r2-r22);
		if(e2<e1){
			x--;
		}
		SDL_RenderDrawPoint(this->renderer,this->px+x,this->py+y);
		SDL_RenderDrawPoint(this->renderer,this->px+x,this->py-y);

		SDL_RenderDrawPoint(this->renderer,this->px-x,this->py+y);
		SDL_RenderDrawPoint(this->renderer,this->px-x,this->py-y);

		SDL_RenderDrawPoint(this->renderer,this->px+y,this->py+x);
		SDL_RenderDrawPoint(this->renderer,this->px+y,this->py-x);

		SDL_RenderDrawPoint(this->renderer,this->px-y,this->py+x);
		SDL_RenderDrawPoint(this->renderer,this->px-y,this->py-x);

		SDL_RenderPresent(this->renderer);
	}
	//SDL_RenderPresent(this->renderer);
}

void Astre::fill(){
	SDL_SetRenderDrawColor(this->renderer,this->r,this->g,this->b,SDL_ALPHA_OPAQUE);
	int x=this->rayon;
	int r2=pow(this->rayon,2);
	int r21=0,r22=0;
	int e1=0,e2=0;
	for(int y=0;y<this->rayon*0.71;y++){
		r21=pow(x,2)+pow(y,2);
		r22=pow(x-1,2)+pow(y,2);
		e1=abs(r2-r21);
		e2=abs(r2-r22);
		if(e2<e1){
			x--;
		}
		SDL_RenderDrawLine(this->renderer,this->px+x,this->py+y,this->px+x,this->py-y);
		SDL_RenderDrawLine(this->renderer,this->px-x,this->py+y,this->px-x,this->py-y);
		SDL_RenderDrawLine(this->renderer,this->px+y,this->py+x,this->px+y,this->py-x);
		SDL_RenderDrawLine(this->renderer,this->px-y,this->py+x,this->px-y,this->py-x);

		//SDL_RenderPresent(this->renderer);
	}
	//SDL_RenderPresent(this->renderer);
}

void Astre::trace(){
	SDL_SetRenderDrawColor(this->renderer,this->r,this->g,this->b,SDL_ALPHA_OPAQUE);
	SDL_RenderDrawPoint(this->renderer,this->px,this->py);
}

double Astre::controleDistance2(double x,double y){
	//this->distance2=pow(this->px-x,2)+pow(this->py-y,2);
	//return this->distance2;
	return pow(this->px-x,2)+pow(this->py-y,2);
}

double Astre::controleDistance2(Astre astre){
	//this->distance2=pow(this->px-astre.getX(),2)+pow(this->py-astre.getY(),2);
	//return this->distance2;
	return pow(this->px-astre.getPX(),2)+pow(this->py-astre.getPY(),2);
}

void Astre::move(double x,double y){
	this->px=x;
	this->py=y;
}

void Astre::move(double dt){
	this->ax=0;
	this->ay=0;
	double a=0;
	for(Astre* astre:this->astres){
		if(this->controleDistance2(*astre)){
			a=G*astre->getMasse()/this->controleDistance2(*astre);
			this->ax=this->ax+a*(astre->getPX()-this->px)/sqrt(this->controleDistance2(*astre));
			this->ay=this->ay+a*(astre->getPY()-this->py)/sqrt(this->controleDistance2(*astre));
		}
	}
	double vx0=this->vx;
	double vy0=this->vy;
	this->vx=this->vx+this->ax*dt;
	this->vy=this->vy+this->ay*dt;
	this->px=this->px+vx0*dt+0.5*this->ax*pow(dt,2);
	this->py=this->py+vy0*dt+0.5*this->ay*pow(dt,2);
}

void Astre::add(Astre* astre){
	this->astres.push_back(astre);
}

void Astre::add(std::vector<Astre*> astres){
	this->astres=astres;
}

void Astre::setColor(int r,int g,int b){
	this->r=r;
	this->g=g;
	this->b=b;
}

/*double Astre::getG(){
	return this->G;
}

void Astre::setG(double G){
	this->G=G;
}*/