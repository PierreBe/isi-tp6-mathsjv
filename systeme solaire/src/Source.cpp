#include <iostream>
#include "SDL2/SDL.h"
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <vector>
#include <math.h>

#include "Astre.h"
#include "Systeme.h"

// g++ src/*.cpp -o bin/prog -I include -L lib -lmingw32 -lSDL2main -lSDL2

#define HEIGHT 1200
#define WIDTH 1200

int main(int argc,char* argv[]){
	SDL_Window* window=NULL;
	SDL_Renderer* renderer=NULL;
	SDL_Event event;
	SDL_bool done=SDL_FALSE;

	srand(time(NULL));

	if(SDL_Init(SDL_INIT_VIDEO)!=0){
		return 1;
	}
	if(SDL_CreateWindowAndRenderer(WIDTH,HEIGHT,0,&window,&renderer)!=0){
		return 2;
	}

	//
	SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);

	// Astre astre(&renderer,px,py,vx,vy,ax,ay,rayon,masse);
	Astre soleil(&renderer,0,0,0,0,0,0,696342e3,1.9891e30);
	soleil.setColor(255,255,0);
	Astre mercure(&renderer,0,69817079e3,38.86e3,0,0,0,2439.7e3,3.3011e23);
	mercure.setColor(255,0,0);
	Astre venus(&renderer,0,108942109e3,34.79e3,0,0,0,6051.8e3,4.8685e24);
	venus.setColor(0,255,255);
	Astre terre(&renderer,0,152097701e3,29.291e3,0,0,0,6378.137e3,5.9736e24);
	terre.setColor(0,0,255);
	Astre lune(&renderer,0,152097701e3+406300e3,29.291e3-0.995e3,0,0,0,1737.4e3,7.3477e22);
	lune.setColor(255,255,255);
	Astre mars(&renderer,0,249228730e3,21.972e3,0,0,0,3396.2e3,641.85e21);
	mars.setColor(255,0,0);
	Astre jupiter(&renderer,0,816620000e3,12.44e3,0,0,0,71492e3,1.8986e27);
	jupiter.setColor(255,128,0);
	Astre saturne(&renderer,0,1503983449e3,9.137e3,0,0,0,60268e3,568.46e24);
	saturne.setColor(255,255,0);
	Astre uranus(&renderer,0,3006318143e3,6.486e3,0,0,0,25559e3,8.6810e25);
	uranus.setColor(255,255,0);
	Astre neptune(&renderer,0,4553946490e3,5.385e3,0,0,0,24764e3,102.43e24);
	neptune.setColor(0,255,255);

	// Astre astre(&renderer,px,py,vx,vy,ax,ay,rayon,masse);
	/*Astre soleil(&renderer,WIDTH/2,HEIGHT/2,0,0,0,0,100,10000000);
	soleil.setColor(255,255,0);
	Astre terre(&renderer,WIDTH/2,HEIGHT-50,0.001,0,0,0,50,50000);
	terre.setColor(0,0,255);
	Astre mars(&renderer,WIDTH-100,HEIGHT/2,0,-0.001,0,0,30,30000);
	mars.setColor(255,0,0);*/

	std::vector<Astre*> astres;

	astres.push_back(&soleil);
	astres.push_back(&mercure);
	astres.push_back(&venus);
	astres.push_back(&terre);
	astres.push_back(&lune);
	astres.push_back(&mars);
	astres.push_back(&jupiter);
	astres.push_back(&saturne);
	astres.push_back(&uranus);
	astres.push_back(&neptune);

	for(Astre* astre:astres){
		astre->add(astres);
	}

	Systeme systeme(&renderer,WIDTH,HEIGHT);
	systeme.add(astres);
	systeme.scale();

	double dt=60;

	/*for(Astre* astre:astres){
		astre->fill();
	}
	SDL_RenderPresent(renderer);*/

	/*for(int i=0;i<60*24*365/2;i++){
		//SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
		//SDL_RenderClear(renderer);
		systeme.compute(dt);
		SDL_RenderPresent(renderer);
	}*/

	while(!done){

		//
		//soleil.move(dt);
		//terre.move(dt);

		/*if(false){
			SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
			SDL_RenderClear(renderer);
			for(Astre* astre:astres){
				//astre->move(dt);
				//astre->fill();
			}
		}else{
			for(Astre* astre:astres){
				//astre->move(dt);
				//astre->trace();
			}
		}*/

		//SDL_SetRenderDrawColor(renderer,0,0,0,SDL_ALPHA_OPAQUE);
		//SDL_RenderClear(renderer);
		//for(int i=0;i<60*24*44;i++)
			systeme.compute(dt);
		SDL_RenderPresent(renderer);

		//std::this_thread::sleep_for(std::chrono::milliseconds(10));

		while(SDL_PollEvent(&event)){
			if(event.type==SDL_QUIT){
				done=SDL_TRUE;
			}
		}
	}

	if(renderer){
		SDL_DestroyRenderer(renderer);
	}
	if(window){
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	return 0;
}