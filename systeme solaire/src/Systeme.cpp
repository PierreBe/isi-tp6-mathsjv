#include "Systeme.h"
#include <iostream>
#include <math.h>

Systeme::Systeme(SDL_Renderer** renderer,int width,int height){
	this->renderer=*renderer;
	this->width=width;
	this->height=height;
}

void Systeme::add(std::vector<Astre*> astres){
	this->systemeBase=astres;
	for(int i=0;i<this->systemeBase.size();i++){
		this->systemeScale.push_back(new Astre(*this->systemeBase[i]));
	}
}

void Systeme::add(Astre* astre){
	this->systemeBase.push_back(astre);
}

void Systeme::scale(){
	int screenLength=this->width<this->height?this->width:this->height;
	double d=0,dMax=0;
	for(Astre* astre:this->systemeBase){
		d=sqrt(astre->getPX()*astre->getPX()+astre->getPY()*astre->getPY())+astre->getRayon();
		if(d>dMax)
			dMax=d;
		//else if(d<-dMax)
		//	dMax=-d;
	}
	//tout/dMax*screenLength;
	this->factor=screenLength/2/dMax;
	/*for(Astre* astre:this->systemeBase){
		astre->setPX(this->width/2+astre->getPX()/dMax*screenLength);
		astre->setPY(this->height/2+astre->getPY()/dMax*screenLength);
		astre->setVX(astre->getVX()/dMax*screenLength/2);
		astre->setVY(astre->getVY()/dMax*screenLength/2);
		astre->setAX(astre->getAX()/dMax*screenLength/2);
		astre->setAY(astre->getAY()/dMax*screenLength/2);
		astre->setRayon(astre->getRayon()/dMax*screenLength/2);
		//astre->setMasse(astre->getMasse()/dMax*screenLength/2);
		astre->setG(astre->getG()/dMax*screenLength/2/dMax*screenLength/2/dMax*screenLength/2);
	}*/
	for(int i=0;i<this->systemeScale.size();i++){
		this->systemeScale[i]->setPX(this->width/2+this->systemeBase[i]->getPX()/dMax*screenLength*2);
		this->systemeScale[i]->setPY(this->height/2+this->systemeBase[i]->getPY()/dMax*screenLength*2);
		//this->systemeScale[i]->setVX(this->systemeBase[i]->getVX()/dMax*screenLength);
		//this->systemeScale[i]->setVY(this->systemeBase[i]->getVY()/dMax*screenLength);
		//this->systemeScale[i]->setAX(this->systemeBase[i]->getAX()/dMax*screenLength);
		//this->systemeScale[i]->setAY(this->systemeBase[i]->getAY()/dMax*screenLength);
		this->systemeScale[i]->setRayon(this->systemeBase[i]->getRayon()/dMax*screenLength);
		//astre->setMasse(astre->getMasse()/dMax*screenLength);
		//astre->setG(astre->getG()/dMax*screenLength/dMax*screenLength/dMax*screenLength);
	}
}

void Systeme::compute(int dt){
	for(int i=0;i<this->systemeScale.size();i++){
		this->systemeBase[i]->move(dt);
		this->systemeScale[i]->setPX(this->width/2+this->systemeBase[i]->getPX()*this->factor);
		this->systemeScale[i]->setPY(this->height/2+this->systemeBase[i]->getPY()*this->factor);
		this->systemeScale[i]->fill();
		//this->systemeScale[i]->setVX(this->systemeBase[i]->getVX()/dMax*screenLength);
		//this->systemeScale[i]->setVY(this->systemeBase[i]->getVY()/dMax*screenLength);
		//this->systemeScale[i]->setAX(this->systemeBase[i]->getAX()/dMax*screenLength);
		//this->systemeScale[i]->setAY(this->systemeBase[i]->getAY()/dMax*screenLength);
		//this->systemeScale[i]->setRayon(this->systemeBase[i]->getRayon()/dMax*screenLength);
		//astre->setMasse(astre->getMasse()/dMax*screenLength);
		//astre->setG(astre->getG()/dMax*screenLength/dMax*screenLength/dMax*screenLength);
	}
}